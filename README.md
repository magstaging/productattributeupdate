# README #

Update product attributes a given property at a time and with a fixed value

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ProductAttributes when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Make a backup of the database before applying the command
php bin/magento mbs:attributes:update is_searchable 0