<?php

namespace Mbs\ProductAttributes\Command;

use Mbs\ProductAttributes\Model\Config\AttributeTranslation\DataInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReadTranslationMapping extends Command
{
    /**
     * @var \Mbs\CustomerGroupConfig\Model\Config\DataInterface
     */
    private $dataConfig;

    public function __construct(
        DataInterface $dataConfig,
        string $name = null
    ) {
        parent::__construct($name);
        $this->dataConfig = $dataConfig;
    }

    protected function configure()
    {
        $this->setName('mbs:config:gettranslation');
        $this->setDescription('Find all the attribute translations in the config');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $attributeTranslations = $this->dataConfig->getAll();

        foreach ($attributeTranslations as $attributeTranslation) {
            $output->writeln('attribute translation attributeTrans: ' . $attributeTranslation['originValue']. ' translation ' . $attributeTranslation['finalValue']);
        }

        $output->writeln('Attribute translations all read');
    }
}