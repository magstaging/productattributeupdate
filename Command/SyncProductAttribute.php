<?php

namespace Mbs\ProductAttributes\Command;

use Magento\Framework\App\Area;
use Magento\Store\Model\Store;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncProductAttribute extends Command
{
    /**
     * @var \Mbs\ProductAttributes\Model\ProductFinder
     */
    private $productFinder;
    /**
     * @var \Mbs\ProductAttributes\Model\AttributeValueHandler
     */
    private $attributeValueHandler;
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $emulation;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    public function __construct(
        \Mbs\ProductAttributes\Model\ProductFinder $productFinder,
        \Mbs\ProductAttributes\Model\AttributeValueHandler $attributeValueHandler,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\App\State $state,
        $name = null
    ) {
        parent::__construct($name);
        $this->productFinder = $productFinder;
        $this->attributeValueHandler = $attributeValueHandler;
        $this->emulation = $emulation;
        $this->state = $state;
    }

    protected function configure()
    {
        $this->setName('mbs:attributes:sync');
        $this->setDescription('Sync an atribute from another original attribute');

        $this->addArgument('attribute_code', InputArgument::REQUIRED);
        $this->addArgument('target_attribute_code', InputArgument::REQUIRED);
        $this->addArgument('limit', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->initialiseAreaCode();
        $this->emulation->startEnvironmentEmulation(Store::DEFAULT_STORE_ID, Area::AREA_ADMINHTML);

        $products = $this->productFinder->getProductThatNeedSyncing(
            $input->getArgument('attribute_code'),
            $input->getArgument('target_attribute_code'),
            (int)$input->getArgument('limit')
        );
        $output->writeln(sprintf(
            'Sync from attribute: %s to attribute to be sync: %s, over %s products',
            $input->getArgument('attribute_code'),
            $input->getArgument('target_attribute_code'),
            (int)$input->getArgument('limit')
        ));

        foreach ($products as $product) {
            $product->setData(
                $input->getArgument('target_attribute_code'),
                $this->attributeValueHandler->getProductValueForTargetAttribute(
                    $product,
                    $input->getArgument('attribute_code'),
                    $input->getArgument('target_attribute_code')
                )
            );
            $product->getResource()->saveAttribute($product, $input->getArgument('target_attribute_code'));
            $output->writeln(sprintf('product sku: %s is now synced', $product->getSku()));
        }

        $this->emulation->stopEnvironmentEmulation();

        $output->writeln('done');
    }

    private function initialiseAreaCode(): void
    {
        try {
            $this->state->setAreaCode(Area::AREA_ADMINHTML);
        } catch (\Exception $e) {
        }
    }
}
