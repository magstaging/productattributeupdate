<?php

namespace Mbs\ProductAttributes\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeProductAttributes extends Command
{
    /**
     * @var \Mbs\ProductAttributes\Model\AttributeFinder
     */
    private $attributeFinder;
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    private $attributeRepository;

    public function __construct(
        \Mbs\ProductAttributes\Model\AttributeFinder $attributeFinder,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->attributeFinder = $attributeFinder;
        $this->attributeRepository = $attributeRepository;
    }

    protected function configure()
    {
        $this->setName('mbs:attributes:update');
        $this->setDescription('Update an atribute with a value');

        $this->addArgument('attribute_property', InputArgument::REQUIRED);
        $this->addArgument('value', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       $productAttributes = $this->attributeFinder->getProductAttribute($input->getArgument('attribute_property'));

        foreach ($productAttributes as $attribute) {
            if ($attribute->getData($input->getArgument('attribute_property'))!= $input->getArgument('value'))
            {
                $attribute->setData($input->getArgument('attribute_property'), $input->getArgument('value'));
                $this->attributeRepository->save($attribute);
                $output->writeln(sprintf('attribute name: %s is not searchable', $attribute->getAttributeCode()));
            }
        }

        $output->writeln('done');
    }
}