<?php

namespace Mbs\ProductAttributes\Model;

use Magento\Store\Model\Store;

class ProductFinder
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var AttributeValueHandler
     */
    private $attributeValueHandler;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Mbs\ProductAttributes\Model\AttributeValueHandler $attributeValueHandler
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->attributeValueHandler = $attributeValueHandler;
    }

    public function getProductThatNeedSyncing(?string $attributeOriginCode, ?string $attributeToSyncCode, int $limit)
    {
        $attributeOrigin = $this->attributeValueHandler->getProductAttribute($attributeOriginCode);
        $attributeToSync = $this->attributeValueHandler->getProductAttribute($attributeToSyncCode);

        $collection = $this->productCollectionFactory->create();
        $collection->addFieldToSelect($attributeOriginCode);
        $collection->addFieldToSelect($attributeToSyncCode);
        $collection->addFieldToFilter($attributeOriginCode, ['neq' => null]);

        $cond = [];
        $cond[] = "at_$attributeToSyncCode.entity_id = e.entity_id";
        $cond[] = $this->getConnection($collection)->quoteInto("at_$attributeToSyncCode.attribute_id=?", $attributeToSync->getId());
        $cond[] = $this->getConnection($collection)->quoteInto("at_$attributeToSyncCode.store_id=?", Store::DEFAULT_STORE_ID);

        $collection->getSelect()->joinLeft(
            ["at_$attributeToSyncCode" => $this->getConnection($collection)->getTableName('catalog_product_entity_' . $attributeOrigin->getBackendType())],
            implode(' AND ', $cond),
            [] /*'at_color_subset_value' => 'at_color_subset.value'*/
        );

        $cond = [];
        $cond[] = "at_$attributeOriginCode.value = origin_attribute.option_id";
        $cond[] = "at_$attributeOriginCode.store_id = origin_attribute.store_id";
        $cond[] = $this->getConnection($collection)->quoteInto("at_$attributeOriginCode.attribute_id=?", $attributeOrigin->getId());
        $cond[] = $this->getConnection($collection)->quoteInto("at_$attributeOriginCode.store_id=?", Store::DEFAULT_STORE_ID);

        $collection->getSelect()->joinLeft(
            ['origin_attribute' => $this->getConnection($collection)->getTableName('eav_attribute_option_value')],
            implode(' AND ', $cond),
            ['origin_label' => 'origin_attribute.value']
        );

        $cond = [];
        $cond[] = "at_$attributeToSyncCode.value = target_attribute.option_id";
        $cond[] = "at_$attributeToSyncCode.store_id = target_attribute.store_id";
        $cond[] = $this->getConnection($collection)->quoteInto("at_$attributeToSyncCode.attribute_id=?", $attributeToSync->getId());
        $cond[] = $this->getConnection($collection)->quoteInto("at_$attributeToSyncCode.store_id=?", Store::DEFAULT_STORE_ID);

        $collection->getSelect()->joinLeft(
            ['target_attribute' => $this->getConnection($collection)->getTableName('eav_attribute_option_value')],
            implode(' AND ', $cond),
            ['target_label' => 'target_attribute.value']
        );

        $collection->getSelect()->where('(origin_attribute.value<>target_attribute.value) or (target_attribute.value is null)');

        $collection->setPageSize($limit);

        //$collection->addIdFilter([27, 24, 25]);

        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection(\Magento\Catalog\Model\ResourceModel\Product\Collection $collection): \Magento\Framework\DB\Adapter\AdapterInterface
    {
        return $collection->getSelect()->getConnection();
    }
}
