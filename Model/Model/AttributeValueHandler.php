<?php

namespace Mbs\ProductAttributes\Model;

use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Mbs\ProductAttributes\Model\Config\AttributeTranslation\DataInterface;

class AttributeValueHandler
{
    /**
     * @var Config
     */
    private $eavConfig;
    /**
     * @var \Magento\Framework\App\Cache
     */
    private $cacheModel;
    /**
     * @var Table
     */
    private $eavSourceTable;
    /**
     * @var Config\AttributeTranslation\DataInterface
     */
    private $translationMappingData;

    public function __construct(
        Config $eavConfig,
        \Magento\Framework\App\Cache $cacheModel,
        Table $eavSourceTable,
        \Mbs\ProductAttributes\Model\Config\AttributeTranslation\DataInterface $translationMappingData
    ) {
        $this->eavConfig = $eavConfig;
        $this->cacheModel = $cacheModel;
        $this->eavSourceTable = $eavSourceTable;
        $this->translationMappingData = $translationMappingData;
    }

    public function getProductValueForTargetAttribute(
        \Magento\Catalog\Model\Product $product,
        ?string $attributeCode,
        ?string $targetAttributeCode
    ) {
        $attributeValueLabel = $this->getAttributeValueLabel($attributeCode, $product);
        return $this->getAttributeValueId($targetAttributeCode, $attributeValueLabel);
    }

    private function getAttributeValueLabel(
        $attributeCode,
        \Magento\Catalog\Model\Product $product
    ) {
        if ($allOptions = $this->getCacheForAttribute($attributeCode)) {
            $value = $product->getData($attributeCode);
            if (is_null($value)) {
                $prodDataValue = $value;
            } else {
                $prodDataValue = '';
                foreach ($allOptions as $option) {
                    if ($prodDataValue) {
                        break;
                    }
                    if ($option['value'] == $value) {
                        $prodDataValue = $option['label'];
                    }
                }

                if ($prodDataValue == '') {
                    $prodDataValue = $this->fallbackIfNoOptionIsFound($attributeCode, $value);
                }
            }
        } else {
            $prodDataValue = $product->getData($attributeCode);
        }

        return $this->parseProductAttributeValue($attributeCode, $prodDataValue);
    }

    private function getAttributeValueId(?string $attributeCode, string $attributeTranslatedLabel)
    {
        $attributeTranslatedLabel = $this->findTranslationForOptionLabel($attributeTranslatedLabel);

        if ($allOptions = $this->getCacheForAttribute($attributeCode)) {
            $prodDataValue = '';
            foreach ($allOptions as $option) {
                if ($prodDataValue) {
                    break;
                }
                if ($option['label'] == $attributeTranslatedLabel) {
                    $prodDataValue = $option['value'];
                }
            }

            if ($prodDataValue == '') {
                $prodDataValue = $this->fallbackIfNoOptionIsFound($attributeCode, $attributeTranslatedLabel);
            }
        } else {
            $prodDataValue = $attributeTranslatedLabel;
        }

        return $prodDataValue;
    }

    private function getCacheForAttribute($attributeCode)
    {
        if (!$cacheData = $this->cacheModel->load($this->getAttributeCacheId($attributeCode))) {
            $productAttribute = $this->getProductAttribute($attributeCode);
            if (is_object($productAttribute) && $productAttribute->usesSource()) {
                $allOptions = $productAttribute->getSource()->getAllOptions();
                $this->cacheModel->save(serialize($allOptions), $this->getAttributeCacheId($attributeCode), ['attribute-data'], 3600);
                $cacheData = $allOptions;
            } else {
                $cacheData = '';
                $this->cacheModel->save($cacheData, $this->getAttributeCacheId($attributeCode), ['attribute-data'], 3600);
            }
        } else {
            $cacheData = unserialize($cacheData);
        }

        return $cacheData;
    }

    public function getNewOptionId(\Magento\Eav\Model\Entity\Attribute $attribute, $label)
    {
        $attributeTable     = $this->eavSourceTable->setAttribute($attribute);
        $allOptions         = $this->eavSourceTable->getAllOptions(false);

        foreach ($allOptions as $option) {
            if ($option['label'] == $label) {
                return $option['value'];
            }
        }

        $count = count($allOptions) +1;

        $data['option']['value']['option_' . $count] = [$label,''];
        $attribute->addData($data);
        $attribute->getResource()->save($attribute);

        $attributeTable        = $this->eavSourceTable->setAttribute($attribute);
        $options               = $this->eavSourceTable->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['label'] == $label) {
                return $option['value'];
            }
        }
    }

    private function parseProductAttributeValue($attributeCode, $prodDataValue)
    {
        switch ($attributeCode) {
            case 'gtin':
                if (strlen($prodDataValue)<10) {
                    $prodDataValue = '';
                }
                break;
            default:
                $prodDataValue = trim($prodDataValue);
        }

        return $prodDataValue;
    }

    private function fallbackIfNoOptionIsFound($attributeCode, $value)
    {
        $productAttribute = $this->getProductAttribute($attributeCode);
        // the value is not currently an option in the system
        $prodDataValue = $this->getNewOptionId($productAttribute, $value);
        $this->clearCacheForAttribute($attributeCode);

        return $prodDataValue;
    }

    /**
     * @param $attributeCode
     */
    private function clearCacheForAttribute($attributeCode)
    {
        $this->cacheModel->remove($this->getAttributeCacheId($attributeCode));
    }

    /**
     * @param string $attributeCode
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute
     */
    public function getProductAttribute($attributeCode)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $eavAttribute */
        $eavAttribute = $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $attributeCode);

        return $eavAttribute;
    }

    /**
     * @param $attributeCode
     * @return string
     */
    private function getAttributeCacheId($attributeCode)
    {
        return 'attribute-data-' . $attributeCode;
    }

    private function findTranslationForOptionLabel(string $attributeTranslatedLabel)
    {
        $allTranslations = $this->translationMappingData->getAll();

        $result = $attributeTranslatedLabel;

        foreach ($allTranslations as $translation) {
            if ($translation['originValue'] == $attributeTranslatedLabel) {
                $result = $translation['finalValue'];
            }
        }

        return $result;
    }
}
