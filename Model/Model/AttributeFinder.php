<?php

namespace Mbs\ProductAttributes\Model;

use Magento\Framework\Api\SearchCriteriaBuilder;

class AttributeFinder
{
    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    private $attributeRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function getProductAttribute(?string $attributeCode)
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        //$searchCriteria->setPageSize(2);

        $attributes = $this->attributeRepository->getList($searchCriteria);

        return $attributes->getItems();
    }
}