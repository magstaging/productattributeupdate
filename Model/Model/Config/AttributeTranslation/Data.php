<?php

namespace Mbs\ProductAttributes\Model\Config\AttributeTranslation;

use Magento\Framework\Config\CacheInterface;
use Magento\Framework\Serialize\SerializerInterface;

class Data extends \Magento\Framework\Config\Data implements DataInterface
{
    /*public function __construct(
        Reader $reader,
        CacheInterface $cache,
        $cacheId = 'attribute_translation_list_cache',
        SerializerInterface $serializer = null
    ) {
        parent::__construct($reader, $cache, $cacheId, $serializer);
    }*/

    /**
     * @inheritDoc
     */
    public function getAll()
    {
        return $this->get('attribute_translation_list');
    }
}
