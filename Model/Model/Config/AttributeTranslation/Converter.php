<?php

namespace Mbs\ProductAttributes\Model\Config\AttributeTranslation;

class Converter implements \Magento\Framework\Config\ConverterInterface
{
    /**
     * @inheritDoc
     */
    public function convert($source)
    {
        $attributeTranslationGroups = $source->getElementsByTagName('group');

        $attributeTranslationInfo = [];
        foreach ($attributeTranslationGroups as $group) {
            $info = [];

            foreach ($group->childNodes as $groupInfo) {
                if ($groupInfo->nodeType != XML_ELEMENT_NODE) {
                    continue;
                }

                $info[$groupInfo->nodeName] = (string)$groupInfo->nodeValue;
            }

            $attributeTranslationInfo[] = $info;
        }

        return ['attribute_translation_list' => $attributeTranslationInfo];
    }
}
