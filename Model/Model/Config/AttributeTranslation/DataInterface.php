<?php

namespace Mbs\ProductAttributes\Model\Config\AttributeTranslation;

interface DataInterface
{
    /**
     * Get configuration of all attribute translation groups
     *
     * @return array
     */
    public function getAll();
}